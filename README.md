# Templater

# Simple, client side vanilla javascript way to have includes for template html files.

## A primitive, client side templating system.  

A combination of javascript's template literals (backticks), `eval()` and `fetch()` (or via XMLHttpRequest methods) makes client side html includes of html snippet files with variable expansion possible.  

V1.2.1 : Has includes inside template files working and a first try at evaluation of javascript in template files.  
  
In your primary html file, say `index.html`, at the bottom in the `body` tag, import and instantiate the `Templater` class: 

~~~~
<script type="module">
    // import { Templater as Templater }   from './assets/javascript/modules/src/templater.js';// easier:
    import { Templater as Templater }   from 'https://unpkg.com/@wowter/templater@1.2.1/assets/javascript/modules/src/templater.js';
    const templaterInstance = new Templater();
</script>
~~~~

To have an include, add a html tag with attribute `data-include` containing the path to the inclusion file: 

~~~~
<div data-include="./templates/top.tmpl.html"></div>
~~~~
  
If you wish to use variables for expansion of place holders in inclusion files, at the top in the body tag add a html tag with attrribute `data-variables`, containing a string with a json object: 
  
~~~~
<div data-variables='{
    "mainCase": "Whoopsie Daisies.", 
    "subCase": "Rimbo De Rukker.", 
    "subSubCase": "Down the great Belows.",
    "bgcolor": "burlywood"
    }'>
</div>
~~~~

Now create html snippet files (without html, body and head tags) that will be your inclusion files. 
Inside your inclusion files place holders can be used: 

~~~~
<div>
    ${variables.mainCase}
    For the subsubdueds!
</div>
~~~~
In the place holder indication `${ }` `variables.mainCase` references "Woopsie Daises.", and will expand as such:  

~~~~
<div>
    Woopsie Daises.
    For the subsubdueds!
</div>
~~~~

Inclusion files are not restricted to html files: 

~~~~
<style data-include="./templates/main.tmpl.css"></style>
~~~~

with `main.tmpl.css` containing

~~~~
body {
    background-color: ${variables.bgcolor};
}
~~~~

will work. 

The javascript code inside `script` tags, or files referenced in `src` attribute in template files are passed through `eval`, but no real testing was done yet.  

See:  
https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Template_literals  
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch  

  
---

